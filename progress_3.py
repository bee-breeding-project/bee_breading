import networkx as nx
import matplotlib.pyplot as plt


def node_dict(x,y,cx,cy):
    return abs(cx - x) + abs(cy - y)
    

def remove_unwanted_nodes(g, m):
    cx, cy = m - 0.5, 2 * m - (m % 2)
    unwanted = []

    for n in g.nodes:
        x, y = n
        if node_dict(x,y,cx,cy) > 2 * m:
            unwanted.append(n)

    for n in unwanted:
        g.remove_node(n)
    return g

m = 4
g = nx.hexagonal_lattice_graph(2 * m - 1, 2 * m - 1,periodic = False,with_positions = True,create_using = None)
pos = nx.get_node_attributes(g,'pos')
g = remove_unwanted_nodes(g, m)

plt.figure(figsize = (9,9))
nx.draw(g,pos = pos,with_labels = True)
plt.axis('scaled')
plt.show()
