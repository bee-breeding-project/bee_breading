import turtle
from math import sqrt

root3 = sqrt(3)
side = 50


def hex_to_rect(coord):
    u, v, w = coord
    print(u, v, w)
    x = u - v / 2 - w / 2
    y = (v - w) * root3 / 2
    print(x, y)
    return x * side, y * side


t = turtle.Turtle()
t.speed(1)

t.up()
coords = [[0, 0, 0], [0, -1, 1], [-1, 0, 1], [-1, 1, 0], [0, 1, -1], [1, 0, -1], [1, -1, 0]]
numbers = [1, 2, 3, 4, 5, 6, 7]

for hexcoord, number in zip(coords, numbers):
    xy = hex_to_rect(hexcoord)
    t.goto(xy)
    t.write(number)
    t.up()

turtle.done()
